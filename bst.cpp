#include <iostream>

using namespace std;

struct element
{
    int klucz;
    element *lewy, *prawy;
};

element *korzen = NULL;

/*
referencja na wskaznik
funkcja majaca mozliwosc zmiany adresu wskaznika
*/
void insert(element *&korzen, int dane)
{
    if (korzen == NULL)
    {
        korzen = new element;
        korzen->lewy = NULL;
        korzen->prawy = NULL;
        korzen->klucz = dane;
    }
    else
    {
        if (dane >= korzen->klucz)
            insert((korzen->prawy), dane);
        else
            insert((korzen->lewy), dane);
    }
}

void inorder(element *korzen)
{
    if (korzen != NULL)
    {
        inorder(korzen->lewy);
        cout << korzen->klucz << " ";
        inorder(korzen->prawy);
    }
}

void preorder(element *korzen)
{
    if (korzen != NULL)
    {
        cout << korzen->klucz << " ";
        preorder(korzen->lewy);
        preorder(korzen->prawy);
    }
}

void postorder(element *korzen)
{
    if (korzen != NULL)
    {
        postorder(korzen->lewy);
        postorder(korzen->prawy);
        cout << korzen->klucz << " ";
    }
}

int main()
{
    int liczba_powtorzen, liczba;

    cout << "Podaj liczbe wezlow do wprowadzenia:\n";
    cin >> liczba_powtorzen;
    while (liczba_powtorzen > 0)
    {
        cin >> liczba;
        insert(korzen, liczba);
        liczba_powtorzen--;
    }

    cout << "Kolejnosc inorder:\n";
    inorder(korzen);
    cout << endl;

    cout << "Kolejnosc preorder:\n";
    preorder(korzen);
    cout << endl;

    cout << "Kolejnosc postorder:\n";
    postorder(korzen);
    cout << endl;

    return 0;
}
